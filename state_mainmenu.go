package main

import (
	"errors"
	"fmt"

	"./engine/resource"

	"github.com/veandco/go-sdl2/sdl"
	// "github.com/veandco/go-sdl2/sdl_ttf"
)

type MainMenuState struct {
	textures map[string]*sdl.Texture
	err      error
}

func (m *MainMenuState) Init() error {
	// Create texture list
	m.textures = make(map[string]*sdl.Texture, 0)
	// Load a basic image in
	if m.textures["image1"], m.err = resource.LoadTexture("./image1.jpg"); m.err != nil {
		return errors.New(fmt.Sprintf("MainMenu: Failed to Load Texture './image1.jpg', error: %s\n", m.err))
	}
	return nil
}

func (m *MainMenuState) Cleanup() error {
	// Clean up loaded textures
	for _, t := range m.textures {
		t.Destroy()
	}
	return nil
}

// Used when temporarily transitioning to another state
func (m *MainMenuState) OnPause() error  { return nil }
func (m *MainMenuState) OnResume() error { return nil }

// Game loop actions
func (m *MainMenuState) OnEvent(event sdl.Event) error { return nil }
func (m *MainMenuState) OnUpdate() error               { return nil }

func (m *MainMenuState) OnDraw(ren *sdl.Renderer) error {
	return resource.DrawTexture(m.textures["image1"], 0, 0, 600, 400, 0, 0)
}
