package main

import (
	// "fmt"

	"./engine"
	"./engine/entities"
	"./engine/gamemap"
	"./engine/types"

	"github.com/veandco/go-sdl2/sdl"
)

type MapTestState struct {
	camera   *gamemap.Camera
	engine   *engine.Engine
	err      error
	gameMap  *gamemap.GameMap
	player   *entities.Player
	textures map[string]*sdl.Texture
}

func (s *MapTestState) Init() error {
	// Configure the state
	s.textures = make(map[string]*sdl.Texture, 0)
	// Set up the player
	s.player = entities.NewPlayer("Antimidget")
	// Set position and size of the player
	s.player.SetPosition(300, 300)
	s.player.SetSize(32, 32)
	s.player.SetSpriteSheet("./assets/characters/default_player.png", 3, types.Point{X: 0, Y: 0})
	s.player.HitBox = sdl.Rect{X: 3, Y: 25, W: 26, H: 7}
	s.player.SetMapLayer("CollisionLayer1")
	// Create a camera to follow our player on the map
	s.camera = gamemap.NewCamera(sdl.Rect{X: 0, Y: 0, W: 600, H: 400})
	// Center the Camera around the player
	s.camera.Target(s.player)
	s.camera.SetTargetMode(gamemap.TargetModeCenter)
	// Create map
	s.gameMap = gamemap.Load("world_1_1.tmx")
	s.gameMap.SetCamera(s.camera)
	s.gameMap.AddEntity(s.player)
	return nil
}

func (s *MapTestState) Cleanup() error {
	// Clean up any loaded textures
	for _, t := range s.textures {
		t.Destroy()
	}
	// Run cleanup on player
	s.player.Cleanup()
	return nil
}
func (s *MapTestState) OnPause() error  { return nil }
func (s *MapTestState) OnResume() error { return nil }

func (s *MapTestState) OnEvent(event sdl.Event) error {
	switch t := event.(type) {
	case *sdl.KeyDownEvent:
		switch t.Keysym.Sym {
		case sdl.K_ESCAPE:
			s.engine.States.Change("mainmenu")
		case sdl.K_UP:
			s.player.SetOrientation(entities.DirectionNorth)
			s.player.Velocity = types.Point{X: 0, Y: -s.player.Speed}
		case sdl.K_RIGHT:
			s.player.SetOrientation(entities.DirectionEast)
			s.player.Velocity = types.Point{X: s.player.Speed, Y: 0}
		case sdl.K_LEFT:
			s.player.SetOrientation(entities.DirectionWest)
			s.player.Velocity = types.Point{X: -s.player.Speed, Y: 0}
		case sdl.K_DOWN:
			s.player.SetOrientation(entities.DirectionSouth)
			s.player.Velocity = types.Point{X: 0, Y: s.player.Speed}
		}
	case *sdl.KeyUpEvent:
		switch t.Keysym.Sym {
		case sdl.K_UP:
			s.player.Velocity.Y = 0
		case sdl.K_RIGHT:
			s.player.Velocity.X = 0
		case sdl.K_LEFT:
			s.player.Velocity.X = 0
		case sdl.K_DOWN:
			s.player.Velocity.Y = 0
		}
	}
	return nil
}

func (s *MapTestState) OnUpdate() error {
	// Update the map
	s.gameMap.OnUpdate()
	// Update the player
	s.player.OnUpdate(s.gameMap)
	return nil
}

func (s *MapTestState) OnDraw(ren *sdl.Renderer) error {
	// Draw the map
	s.gameMap.OnDraw()
	// Draw the camera frame
	s.camera.DrawFrame()
	return nil
}
