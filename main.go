package main

import (
	"./engine"
)

func main() {
	game := engine.New()
	if err := game.Init("Engine Test v2.0", 800, 600); err != nil {
		panic(err)
	}
	defer game.OnCleanUp()

	// Add the currently aviable game states
	if err := game.States.Add("mainmenu", &MainMenuState{}); err != nil {
		game.Logger.Print(err)
	}
	if err := game.States.Add("maptest", &MapTestState{}); err != nil {
		game.Logger.Print(err)
	}
	// Change to our first game state
	game.States.Change("maptest")

	// Main game loop
	for game.IsRunning() {
		// Hand player input
		game.OnEvent()
		// Update game logic
		game.OnUpdate()
		// Draw to the screen
		game.OnDraw()
	}
}
