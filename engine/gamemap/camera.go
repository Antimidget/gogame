package gamemap

import (
	"../resource"

	"github.com/veandco/go-sdl2/sdl"
)

type TargetMode int

const (
	TargetModeNormal TargetMode = iota
	TargetModeCenter
)

type Targetable interface {
	X() int32
	Y() int32
	Width() int32
	Height() int32
}

type Camera struct {
	// This is the window size, notmally the same size as the render window
	window sdl.Rect
	// This is a slightly larger window view, used to draw a little bit
	// futher to avoid flicker on the edge of the screen when moving and
	// drawing tiles/sprites
	OuterRect sdl.Rect
	// Target the camera follows
	target     Targetable
	targetMode TargetMode
}

func NewCamera(size sdl.Rect) *Camera {
	return &Camera{window: size}
}

func (c *Camera) SetX(x int32) { c.window.X = x }
func (c *Camera) SetY(y int32) { c.window.Y = y }

func (c *Camera) X() int32 {
	if c.target != nil {
		if c.targetMode == TargetModeCenter {
			return c.target.X() - (-(c.target.Width() / 2) + (c.window.W / 2))
		}
		return c.target.X()
	}
	return c.window.X
}

func (c *Camera) Y() int32 {
	if c.target != nil {
		if c.targetMode == TargetModeCenter {
			return c.target.Y() - (-(c.target.Height() / 2) + (c.window.H / 2))
		}
		return c.target.Y()
	}
	return c.window.Y
}

// Move the camera by this many units
func (c *Camera) Move(x, y int32) {
	c.window.X += x
	c.window.Y += y
}

// Move to a specific location
func (c *Camera) SetPosition(x, y int32) {
	c.window.X = x
	c.window.Y = y
}

func (c *Camera) SetWidth(w int32)  { c.window.W = w }
func (c *Camera) SetHeight(h int32) { c.window.H = h }
func (c *Camera) Width() int32      { return c.window.W }
func (c *Camera) Height() int32     { return c.window.H }

func (c *Camera) SetTargetMode(m TargetMode) { c.targetMode = m }
func (c *Camera) TargetMode() TargetMode     { return c.targetMode }
func (c *Camera) Target(t Targetable)        { c.target = t }

// Does the following coords from the rect intercept with the camera
func (c *Camera) Intercepts(rect sdl.Rect) bool {
	if !(rect.X > (c.window.X+c.window.W) && (rect.X+rect.W) < c.window.X && rect.Y > (c.window.Y+c.window.H) && (rect.Y+rect.H) < c.window.Y) {
		return true
	}
	return false
}

// Used to debug if the camera is at the correct position.
func (c *Camera) DrawFrame() {
	resource.DrawBox(c.window, sdl.Color{R: 255, G: 0, B: 0, A: 255})
}

func (c *Camera) OnUpdate() {
	// Update the outer rect to be just larer than then what would fix inside
	// the renderer dimentions.
	c.OuterRect = sdl.Rect{X: c.window.X - 1,
		Y: c.window.Y - 1,
		W: c.window.W + 1,
		H: c.window.H + 1}
}
