package gamemap

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"../resource"
	"../types"

	"github.com/salviati/go-tmx/tmx"
	"github.com/veandco/go-sdl2/sdl"
)

const (
	ASSETS_MAPS_PATH     = "./assets/maps/%s"
	ASSETS_TILESETS_PATH = "./assets/tilesheets/%s"
)

var (
	MissingRenderer = errors.New("No Renderer was provided on GameMap Creation.")
	MissingMapData  = errors.New("Failed to find TMX map data.")
)

// Entities that can be drawn on the map, normally handled by the map
type Entity interface {
	MapLayer() string
	X() int32
	Y() int32
	OnDraw(offset types.Point)
	OnUpdate(gm *GameMap)
}

type GameMap struct {
	err      error
	mapFile  string
	mapData  *tmx.Map
	tileSets map[string]*sdl.Texture
	camera   *Camera
	// cameraTiles is a rect that contains the x & y coords of the first tile
	// and how many tiles wide & high fix into the camera
	cameraTiles sdl.Rect
	entities    []Entity
}

func Load(mapFilePath string) *GameMap {
	m := &GameMap{mapFile: mapFilePath,
		tileSets: make(map[string]*sdl.Texture, 0),
		entities: make([]Entity, 0)}
	// Load map data from file
	m.loadMapFile()
	// Load tile sets for the map
	m.loadTilesheets()
	// Load map entities
	//g.loadMapEntities()
	// Load Map NPC's
	//g.loadNPCs()
	return m
}

func (g *GameMap) SetCamera(c *Camera) { g.camera = c }
func (g *GameMap) Camera() *Camera     { return g.camera }

func (g *GameMap) loadMapFile() {
	var f *os.File
	if f, g.err = os.Open(fmt.Sprintf(ASSETS_MAPS_PATH, g.mapFile)); g.err != nil {
		fmt.Fprintf(os.Stderr, "GameMap: %s\n", g.err)
		return
	}
	defer f.Close()
	// Parse TMX map
	if g.mapData, g.err = tmx.Read(f); g.err != nil {
		fmt.Fprintf(os.Stderr, "GameMap: %s\n", g.err)
	}
	fmt.Printf("Version: %s\n", g.mapData.Orientation)
}

func (g *GameMap) loadTilesheets() {
	fmt.Println("Loading tilesheets")
	if g.mapData == nil {
		fmt.Fprintf(os.Stderr, "GameMap: %s\n", MissingMapData)
		return
	}
	// Loop over tilesets defined in tmx data and load the tileset data
	for _, tileset := range g.mapData.Tilesets {
		// Load the tile set
		g.tileSets[tileset.Name], g.err = resource.LoadTexture(fmt.Sprintf(ASSETS_TILESETS_PATH, tileset.Image.Source))
		if g.err != nil {
			fmt.Fprintf(os.Stderr, "GameMap.TileSet: %s -> %s\n", tileset.Name, g.err)
		}
	}
}

func (g *GameMap) AddEntity(e Entity) { g.entities = append(g.entities, e) }

func (g *GameMap) OnUpdate() {
	g.camera.OnUpdate()

	// Build a rect from the camera that lists the tiles on the map we are to show.
	cr := sdl.Rect{X: g.camera.X(), Y: g.camera.Y(), W: g.camera.Width(), H: g.camera.Height()}
	g.cameraTiles = sdl.Rect{X: cr.X / int32(g.mapData.TileWidth),
		Y: cr.Y / int32(g.mapData.TileHeight),
		W: (cr.W+1)/int32(g.mapData.TileWidth) + 2,
		H: ((cr.H + 1) / int32(g.mapData.TileHeight)) + 2}

	// Update Entities
	for _, entity := range g.entities {
		entity.OnUpdate(g)
	}
}

func (g *GameMap) OnDraw() {
	// Loop over each layer in the map
	for _, layer := range g.mapData.Layers {
		// Loop over entities
		for _, entity := range g.entities {
			if entity.MapLayer() == layer.Name {
				entity.OnDraw(types.Point{X: g.camera.X(), Y: g.camera.Y()})
			}
		}
		// Skip the layers containing the keyword 'Collision'
		if strings.Contains(layer.Name, "Collision") {
			continue
		}
		// Loop over the camera view and draw tiles for this layer that are in view.
		for x := g.cameraTiles.X; x < (g.cameraTiles.X + g.cameraTiles.W); x++ {
			for y := g.cameraTiles.Y; y < (g.cameraTiles.Y + g.cameraTiles.H); y++ {
				// skip drawing if outside the map
				if x < 0 || x > int32(g.mapData.Width-1) || y < 0 || y > int32(g.mapData.Height) {
					continue
				}
				// Get the tile number
				tileNum := x + (y * int32(g.mapData.Width))
				// Skip invalid tile numbers
				if tileNum < 0 || tileNum > int32(len(layer.DecodedTiles)-1) {
					continue
				}
				// Get Tile
				tile := layer.DecodedTiles[tileNum]
				// Confirm the tile requires drawing
				if tile.IsNil() {
					continue
				}
				// Work out where the tile should be drawn in the window
				dstY := (y * int32(tile.Tileset.TileHeight)) - g.camera.Y()
				dstX := (x * int32(tile.Tileset.TileWidth)) - g.camera.X()
				// fmt.Printf("dstX: %d, dstY: %d\n", dstX, dstY)
				// Work out the tile location on the tile set
				srcX := (int32(tile.ID) % int32(tile.Tileset.Image.Width/tile.Tileset.TileWidth)) * int32(tile.Tileset.TileWidth)
				srcY := (int32(tile.ID) / int32(tile.Tileset.Image.Width/tile.Tileset.TileWidth)) * int32(tile.Tileset.TileHeight)
				// Draw the tile to the screen
				err := resource.DrawTexture(g.tileSets[tile.Tileset.Name],
					srcX,
					srcY,
					int32(tile.Tileset.TileWidth),
					int32(tile.Tileset.TileHeight),
					dstX,
					dstY)
				if err != nil {
					fmt.Println(err)
				}
			}
		}
	}
}

// Check if there is something solid in the given rectangle
func (g *GameMap) SolidAt(layer string, rect sdl.Rect) bool {
	for _, l := range g.mapData.Layers {
		// Look for the layer we want
		if l.Name != layer {
			continue
		}
		// Found our layer, lets find if our rect hits something
		for x := g.cameraTiles.X; x < (g.cameraTiles.X + g.cameraTiles.W); x++ {
			for y := g.cameraTiles.Y; y < (g.cameraTiles.Y + g.cameraTiles.H); y++ {
				// skip drawing if outside the map
				if x < 0 || x > int32(g.mapData.Width-1) || y < 0 || y > int32(g.mapData.Height) {
					continue
				}
				// Get tile number
				tileNum := x + (y * int32(g.mapData.Width))
				// Skip invalid tile numbers
				if tileNum < 0 || tileNum > int32(len(l.DecodedTiles)-1) {
					continue
				}
				// Get Tile
				tile := l.DecodedTiles[tileNum]
				// Confirm the tile requires drawing
				if tile.IsNil() {
					continue
				}
				// Work out where the tile would be drawn on the screen
				tY := (y * int32(tile.Tileset.TileHeight))
				tX := (x * int32(tile.Tileset.TileWidth))
				// Check for intercepts on our tile
				if rect.HasIntersection(&sdl.Rect{X: tX, Y: tY, W: int32(tile.Tileset.TileWidth), H: int32(tile.Tileset.TileHeight)}) {
					return true
				}

			}
		}
	}
	return false
}

// Check if any type of collision happens.
func (g *GameMap) CollisionAt(rect sdl.Rect) bool { return false }
