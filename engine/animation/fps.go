package animation

import (
	"github.com/veandco/go-sdl2/sdl"
)

// Frames Per Second controller, designed to help with updating items correctly
// no matter what the speed the game runs at.
type Fps struct {
	oldTime     uint32
	lastTime    uint32
	numFrames   uint32
	frames      uint32
	speedFactor float64
	limit       uint32
}

func (f *Fps) SetLimit(l uint32)    { f.limit = l }
func (f *Fps) Rate() int            { return int(f.limit) }
func (f *Fps) SpeedFactor() float64 { return f.speedFactor }
func (f *Fps) OldTime() uint32      { return f.oldTime }
func (f *Fps) LastTime() uint32     { return f.lastTime }

func (f *Fps) OnUpdate() {
	if f.oldTime+1000 < sdl.GetTicks() {
		f.oldTime = sdl.GetTicks()
		f.numFrames = f.frames
		f.frames = 0
	}

	d := sdl.GetTicks() - f.lastTime
	f.speedFactor = (float64(d) / 1000) * 32.0
	f.lastTime = sdl.GetTicks()
	f.frames++
}
