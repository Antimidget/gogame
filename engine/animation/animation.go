package animation

import (
	"github.com/veandco/go-sdl2/sdl"
)

// Animation helper, designed to be used with entity items, like the players
// character and map entities like water, etc
type Animation struct {
	currentFrame   int32
	incrementValue int32
	frameRate      uint32 // milliseconds
	oldTime        uint32
	MaxFrames      int32
}

func New(maxFrames int32) *Animation {
	return &Animation{incrementValue: 1,
		frameRate: 100, // Frames should only update every 100 milliseonds.
		MaxFrames: maxFrames}
}

func (a *Animation) CurrentFrame() int32   { return a.currentFrame }
func (a *Animation) FrameRate() uint32     { return a.frameRate }
func (a *Animation) SetFrameRate(r uint32) { a.frameRate = r }

func (a *Animation) SetFrame(f int32) {
	// If input less than 0, assign currentFrame to 0
	if f <= 0 {
		a.currentFrame = 0
		return
	}
	// If input greater than max frame value, set current to max frames value.
	if f >= a.MaxFrames {
		a.currentFrame = a.MaxFrames
		return
	}
	a.currentFrame = f
}

func (a *Animation) OnUpdate() {
	if a.oldTime+a.frameRate > sdl.GetTicks() {
		return
	}
	a.oldTime = sdl.GetTicks()
	a.currentFrame += a.incrementValue
	if a.currentFrame >= a.MaxFrames {
		a.currentFrame = 0
	}
}
