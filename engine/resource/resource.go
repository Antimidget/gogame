package resource

import (
	"archive/zip"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_image"
)

type Resource struct {
	renderer *sdl.Renderer
}

var resource *Resource

// Singleton instance of the resource loader
func Instance() *Resource {
	if resource == nil {
		resource = &Resource{}
	}
	return resource
}

func (r *Resource) Renderer() *sdl.Renderer       { return r.renderer }
func (r *Resource) SetRenderer(ren *sdl.Renderer) { r.renderer = ren }

func LoadResourceFile(f string) error {
	// Load zip file
	r, err := zip.OpenReader(f)
	if err != nil {
		return err
	}
	defer r.Close()

	return nil
}

func LoadTexture(f string) (*sdl.Texture, error) { return img.LoadTexture(Instance().Renderer(), f) }
func DrawTexture(tex *sdl.Texture, srcX, srcY, width, height, dstX, dstY int32) error {
	src := sdl.Rect{srcX, srcY, width, height}
	dst := sdl.Rect{dstX, dstY, width, height}
	if err := Instance().Renderer().Copy(tex, &src, &dst); err != nil {
		return err
	}
	return nil
}

func DrawBox(rect sdl.Rect, color sdl.Color) {
	// Backup current draw color
	r, g, b, a, _ := Instance().Renderer().GetDrawColor()
	// Set the color for our drawing
	Instance().Renderer().SetDrawColor(color.R, color.G, color.B, color.A)
	// Draw the supplied rect
	Instance().Renderer().DrawRect(&rect)
	// Reset the draw color
	Instance().Renderer().SetDrawColor(r, g, b, a)
}
