package engine

import ()

type BattleState int

const (
	BattleStart BattleState = iota + 1
	BattlePlayerChoice
	BattleEnemyChoice
	BattleLose
	BattleWin
)

type Battle struct {
	currentState BattleState
}

func (b *Battle) Init() {
	b.currentState = BattleStart
}

func (b *Battle) OnUpdate() {
	switch b.currentState {
	case BattleStart:
		break
	case BattlePlayerChoice:
		break
	case BattleEnemyChoice:
		break
	case BattleLose:
		break
	case BattleWin:
		break
	}
}

func (b *Battle) ChangeState(s BattleState) { b.currentState = s }
func (b *Battle) NextState() {
	switch b.currentState {
	case BattleStart:
		b.currentState = BattlePlayerChoice
		break
	case BattlePlayerChoice:
		b.currentState = BattleEnemyChoice
		break
	case BattleEnemyChoice:
		b.currentState = BattlePlayerChoice
		break
	case BattleLose:
		b.currentState = BattleStart
		break
	case BattleWin:
		b.currentState = BattleStart
		break
	}
}
