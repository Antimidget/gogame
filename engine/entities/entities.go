package entities

import ()

type Orientation uint

const (
	DirectionSouth Orientation = iota
	DirectionWest
	DirectionEast
	DirectionNorth
)
