package entities

import (
	"../animation"
	"../gamemap"
	"../resource"
	"../types"

	"github.com/veandco/go-sdl2/sdl"
)

type Player struct {
	Name        string
	err         error
	animation   *animation.Animation
	fps         *animation.Fps
	orientation Orientation
	// Bounds contains the X, Y positions & the Width & Height of the player
	Bounds sdl.Rect
	// Velocity is set when one of the movement keys is pressed down, it
	// controls what direction we are moving.
	Velocity     types.Point
	HitBox       sdl.Rect
	Sprite       *sdl.Texture
	SpriteFile   string
	SpriteOffset types.Point
	MaxFrames    int

	// the raw values of the players location
	rawX float64
	rawY float64
	// Speed the player moves across the map
	Speed int32
	// When drawing on the map, draw on this layer
	mapLayerName string
}

func NewPlayer(name string) *Player {
	return &Player{Name: name,
		fps:   &animation.Fps{},
		Speed: 3}
}

func (p *Player) Orientation() Orientation     { return p.orientation }
func (p *Player) SetOrientation(o Orientation) { p.orientation = o }
func (p *Player) X() int32                     { return p.Bounds.X }
func (p *Player) Y() int32                     { return p.Bounds.Y }
func (p *Player) Width() int32                 { return p.Bounds.W }
func (p *Player) Height() int32                { return p.Bounds.H }

func (p *Player) SetPosition(x, y int32) {
	p.Bounds.X = x
	p.Bounds.Y = y
	p.rawX = float64(x)
	p.rawY = float64(y)
}

func (p *Player) SetSize(w, h int32) {
	p.Bounds.W = w
	p.Bounds.H = h
}

func (p *Player) MapLayer() string         { return p.mapLayerName }
func (p *Player) SetMapLayer(layer string) { p.mapLayerName = layer }

func (p *Player) SetSpriteSheet(file string, maxFrames int32, offset types.Point) error {
	p.SpriteFile = file
	p.animation = animation.New(maxFrames)
	p.SpriteOffset = offset
	p.Sprite, p.err = resource.LoadTexture(p.SpriteFile)
	return p.err
}

func (p *Player) OnAnimate() {
	if p.Velocity.X == 0 && p.Velocity.Y == 0 {
		// Player isn't moving, don't animate
		return
	}
	p.animation.OnUpdate()
}

func (p *Player) OnUpdate(gm *gamemap.GameMap) {
	p.fps.OnUpdate()
	p.OnAnimate()
	p.OnMove(gm)
}

func (p *Player) OnMove(gm *gamemap.GameMap) {
	if p.Velocity.X == 0 && p.Velocity.Y == 0 {
		// Player isn't moving, no need to move
		return
	}
	// Work out the position the player will move into
	newX := p.rawX + (float64(p.Velocity.X) * p.fps.SpeedFactor())
	newY := p.rawY + (float64(p.Velocity.Y) * p.fps.SpeedFactor())
	x := int32(newX)
	y := int32(newY)
	// build rect player hitbox will be in once they move.
	pRect := sdl.Rect{X: x + p.HitBox.X, Y: y + p.HitBox.Y, W: p.HitBox.W, H: p.HitBox.H}
	// Check if this rect would collide with anything on the map.
	if gm.SolidAt(p.MapLayer(), pRect) {
		return
	}
	// Player can move, so update it values
	p.rawX = newX
	p.rawY = newY
	p.Bounds = sdl.Rect{X: x, Y: y, W: p.Bounds.W, H: p.Bounds.H}
}

func (p *Player) OnDraw(offset types.Point) {
	// Draw to the renderer
	resource.DrawTexture(p.Sprite,
		// Source on the tile sheet
		(p.animation.CurrentFrame()*p.Bounds.W)+p.SpriteOffset.X,
		(int32(p.Orientation())*p.Bounds.H)+p.SpriteOffset.Y,
		// Size of the player's sprite
		p.Bounds.W,
		p.Bounds.H,
		// Camera location of the player
		(p.Bounds.X - offset.X),
		(p.Bounds.Y - offset.Y))

	// TEMP: Draw hit box for player
	pRect := sdl.Rect{X: p.Bounds.X + p.HitBox.X - offset.X,
		Y: p.Bounds.Y + p.HitBox.Y - offset.Y,
		W: p.HitBox.W,
		H: p.HitBox.H}

	resource.DrawBox(pRect, sdl.Color{R: 0, B: 255, G: 0, A: 255})
}

func (p *Player) Cleanup() {}
