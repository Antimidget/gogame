package ui

import (
	"../resource"

	"github.com/veandco/go-sdl2/sdl"
)

type ButtonState int

const (
	StateMouseOut ButtonState = iota
	StateMouseOver
	StateMouseClick
	StateDisabled
)

type OnClickCallback func() error

type Button struct {
	err          error
	currentState ButtonState
	width        int32
	height       int32
	col          int32
	row          int32
	src          string
	img          sdl.Texture

	leftClickFunc  OnClickCallback
	rightClickFunc OnClickCallback

	X int32
	Y int32
}

func NewButton(imgPath string, x, y, w, h int32) (*Button, error) {
	b := &Button{width: w,
		height: h,
		col:    0,
		row:    0,
		src:    imgPath,
		X:      x,
		Y:      y}
	b.img, b.err = resource.LoadTexture(b.src)
	if b.err != nil {
		return nil, b.err
	}
	return b, nil
}

func (b *Button) Width() int32  { return b.width }
func (b *Button) Height() int32 { return b.height }

func (b *Button) OnLeftClick(fn OnClickCallback)  { b.leftClickFunc = fn }
func (b *Button) OnRightClick(fn OnClickCallback) { b.rightClickFunc = fn }

func (b *Button) IsMouseOver(mX, my int32) bool {
	if mX >= b.X && mX <= (b.X+b.Width()) && mY >= b.Y && mY <= (b.Y+b.Height()) {
		return true
	}
	return false
}

func (b *Button) Enable()  { b.currentState = StateMouseOut }
func (b *Button) Disable() { b.currentState = StateDisabled }

func (b *Button) OnDraw(ren *sdl.Renderer) {}
func (b *Button) OnUpdate(mX, mY int32, mouseClick bool) error {
	if b.currentState == StateDisabled {
		// Button is disabled
		return
	}

	if !b.IsMouseOver(mX, mY) {
		// Set state of button to mouse out
		b.currentState = StateMouseOut
		return
	}
	// Set state to mouse Over
	b.currentState = StateMouseOver
	// Check if user is clicking button
	if mouseClick {
		b.currentState = StateMouseClick
		b.leftClickFunc()
	}
}

func (b *Button) OnCleanUp() {
	// Destroy the loaded texture
	b.img.Destroy()
}
