// Used the handle the management of the states a game can be in, ex: main menu,
// or world map states. There is also an interface here that all game states
// should implement.
package states

import (
	"github.com/veandco/go-sdl2/sdl"
)

// Game State interface should be implemented by all game states you wish to
// use.
type State interface {
	// Set up and destroy the state
	Init() error
	Cleanup() error
	// Used when temporarily transitioning to another state
	OnPause() error
	OnResume() error
	// Game loop actions
	OnEvent(event sdl.Event) error
	OnUpdate() error
	OnDraw(ren *sdl.Renderer) error
}

// State manager handles the switching and passing through of information to the
// various game states.
type StateManager struct {
	states       map[string]State
	currentState State
}

func NewStateManager() *StateManager {
	return &StateManager{states: make(map[string]State, 0)}
}

// Forces th cleanup of all active game states
func (s *StateManager) CleanUp() {
	// Unset current state
	s.currentState = nil
	// Run cleanups for each of the states
	for _, state := range s.states {
		state.Cleanup()
	}
}

func (s *StateManager) Add(name string, state State) error {
	s.states[name] = state
	return s.states[name].Init()
}
func (s *StateManager) Remove(name string)             { s.states[name].Cleanup() }
func (s *StateManager) Update() error                  { return s.currentState.OnUpdate() }
func (s *StateManager) OnDraw(ren *sdl.Renderer) error { return s.currentState.OnDraw(ren) }
func (s *StateManager) Change(name string) {
	// Pause current running state if one exists.
	if s.currentState != nil {
		s.currentState.OnPause()
	}
	// Update the pointer to the running state.
	s.currentState = s.states[name]
	// Make sure the new running state loads back up.
	s.currentState.OnResume()
}

func (s *StateManager) OnEvent(e sdl.Event) error {
	if s.currentState == nil {
		return nil
	}
	return s.currentState.OnEvent(e)
}

func (s *StateManager) OnUpdate() {
	if s.currentState == nil {
		return
	}
	s.currentState.OnUpdate()
}
