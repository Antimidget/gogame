package engine

import (
	"./animation"
	"./resource"
	"./states"
	"log"
	"os"
	"runtime"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_mixer"
	"github.com/veandco/go-sdl2/sdl_ttf"
)

type Engine struct {
	running bool

	Logger   *log.Logger
	States   *states.StateManager
	Window   *sdl.Window
	Renderer *sdl.Renderer
	Fps      *animation.Fps
}

func New() *Engine {
	return &Engine{running: true,
		States: states.NewStateManager(),
		Logger: log.New(os.Stdout, "Engine", log.LstdFlags|log.Lshortfile)}
}

// Initial the game window, renderer, fonts & audio
func (e *Engine) Init(title string, width, height int) (err error) {
	// Lock engine to single thread, this is keep the renderer in check becuase
	// of the 'C' calls
	runtime.LockOSThread()
	// Inialize SDL2
	if err = sdl.Init(sdl.INIT_AUDIO | sdl.INIT_VIDEO | sdl.INIT_TIMER); err != nil {
		e.Logger.Printf("Failed to initalize game, error: %s\n", err)
		return
	}
	// Create the window
	if e.Window, err = sdl.CreateWindow(title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, width, height, sdl.WINDOW_OPENGL); err != nil {
		e.Logger.Printf("Failed to create game window, error: %s\n", err)
		return
	}
	// Initalize Renderer (Frame buffer)
	if e.Renderer, err = sdl.CreateRenderer(e.Window, -1, sdl.RENDERER_ACCELERATED|sdl.RENDERER_TARGETTEXTURE); err != nil {
		e.Logger.Printf("Failed to create game renderer, error: %s\n", err)
		return
	}
	// Set up background color (black)
	e.Renderer.SetDrawColor(0, 0, 0, 255)
	// Update resource manager to use this renderer
	resource.Instance().SetRenderer(e.Renderer)
	// Load in Font handing (TTF libs)
	if err = ttf.Init(); err != nil {
		e.Logger.Printf("Failed to initlize TTF library, error: %s\n", err)
		return
	}
	// Initalize Audio
	if err = mix.Init(mix.INIT_OGG); err != nil {
		e.Logger.Printf("Failed to initalize Audio Library, error: %s\n", err)
		return
	}
	// Init FPS controller
	e.Fps = &animation.Fps{}
	// Finished creating everything we need to start.
	return nil
}

// Clean up the game
func (e *Engine) OnCleanUp() {
	// Defer sdl2 cleanups
	// Destroy SDL 2 loaded libraries
	defer sdl.Quit()
	// Close Audio
	defer mix.Quit()
	defer mix.CloseAudio()
	// Clean up TTF libs
	defer ttf.Quit()

	// Clean up game states
	e.States.CleanUp()
	// Destroy the renderer
	e.Renderer.Destroy()
	// Close the game window
	e.Window.Destroy()
}

// Returns true if we are still running
func (e *Engine) IsRunning() bool { return e.running }

// Tell engine we wish to quit
func (e *Engine) Quit() { e.running = false }

// When we maximise the game window, run this method.
func (e *Engine) OnMaximise() {}

// When we minimize the game window, run this method.
func (e *Engine) OnMinimize() {}

// When the game window is resized, run this method.
func (e *Engine) OnResize() {}

// On hardware event (keyboard, mouse, etc) notify the current game state.
func (e *Engine) OnEvent() {
	var event sdl.Event
	for event = sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		// Handle global events here, Close, Minimize, Maximize, etc...
		switch event.(type) {
		case *sdl.QuitEvent:
			e.Quit()
			return
		}

		// Pass event through to the game state OnEvent method.
		if err := e.States.OnEvent(event); err != nil {
			e.Logger.Printf("OnEvent failed: %s\n", err)
		}
	}
}

// On each game loop run this method.
func (e *Engine) OnUpdate() {
	// Update FPS control
	e.Fps.OnUpdate()
	// Update current Game state
	e.States.OnUpdate()
}

// At the end of the game loop, update the display with this method.
func (e *Engine) OnDraw() {
	// Clear the screen
	e.Renderer.Clear()
	// Draw to the renderer/frame buffer
	if err := e.States.OnDraw(e.Renderer); err != nil {
		panic(err)
	}
	// Present the renderer to the game window so the user can see the changes
	e.Renderer.Present()
	// Wait for a deplay in sdl, this allows other programs to get in and use
	// the CPU, that way our game doesn't hold onto the CPU while doing nothing.
	sdl.Delay(1)
}
