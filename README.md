# README #

http://shadowcompanyelite.com

This is the basics of a simple game engine, based on the following [tutorial](http://gamedevgeek.com/tutorials/managing-game-states-in-c) and written using SDL2 & GoLang

## The following linux packages are required to build this: ##
* libsdl2-2.0-0
* libsdl2-dev
* libsdl2-image-2.0-0
* libsdl2-image-dev
* libsdl2-mixer-2.0-0
* libsdl2-mixer-dev
* libsdl2-net-2.0-0
* libsdl2-net-dev
* libsdl2-ttf-2.0-0
* libsdl2-ttf-dev

##You also need to `go get` the following ##
* github.com/veandco/go-sdl2/sdl
* github.com/veandco/go-sdl2/sdl_image
* github.com/veandco/go-sdl2/sdl_ttf
* github.com/salviati/go-tmx/tmx

### What is this repository for? ###
This is an early port of a game that was started in c++, i've scraped most of it keeping just the basic logic and added some new things will re-implementing it in Go.
Designed around a JRPG a friend was building before he passed away. This game is a way of immortalizing not only SCE_SwiffinGriffin but also our other fallen friend
who we have had the privlage of serving with, gaming with and hanging out with.

Design & storyline of this game is supplied by our community, those who knew our firends and the families of the fallen.

### Contribution guidelines ###

I wellcome all comments, suggestions, designed ideas, story ideas from anyone who wants to help out, send me an email at sce.anti@gmail.com or visit our forums http://www.shadowcompanyelite.com/main/index.php?/topic/8731-project-sce/
we are always welcoming to new players & firends.