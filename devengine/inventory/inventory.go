package inventory

import (
	"encoding/json"
	"os"

	"../entity"
)

type Item struct {
	// Pointer to the item we have.
	Item *entity.Entity
	// Quanity of that item in our inventory.
	Quanty int
}

type Inventory struct {
	items []Item
}

func New() *Inventory { return &Inventory{items: make([]Item, 0)} }

func Load(file string) error {
	raw, err := os.Open(file)
	if err != nil {
		return err
	}
	// Need to loop over items in the list and decode them into the correct
	// entity types (Item, Armor or Weapon)
	return nil
}

func (i *Inventory) Items() []Item { return i.items }

// Adds an item into the inventory
func (i *Inventory) Add(item *entity.Entity, qty int) {
	// Check if item already exists here, if so, update the total
	for idx, itm := range i.items {
		if itm.Item.Id() == item.Id() {
			i.items[idx].Quanty += qty
			return
		}
	}
	// Since item didn;t exist, append the item
	i.items = append(i.items, Item{Item: item, Qty: qty})
}

// Attempts to remove an item from the Inventory
func (i *Inventory) Remove(item *entity.Entity, count int) {
	// Look for the item and decrease the quanty if found
	for _, itm := range i.items {
		if itm.Item.Id() == item.Id() {
			itm.Quanty -= count
		}
	}
	// Check for items that should no longer be in the inventory (no items left)
	for idx, itm := range i.items {
		if itm.Quanty <= 0 {
			// Item had a value of 0 or less, remove it from the list
			i.items = append(i.items[:idx], i.items[idx+1:]...)
		}
	}
}

// Returns the count of all the items in the inventory.
func (i *Inventory) Count() int {
	count := 0

	for _, itm := range i.items {
		count += itm.Quanty
	}
	return count
}

// Returns the quanty count of the item.
func (i *Inventory) Quanty(item *entity.Entity) int {
	for _, itm := range i.items {
		if itm.Item.Id() == item.Id() {
			return itm.Quanty
		}
	}
	return 0
}

// Returns the item at the supplied index value.
func (i *Inventory) Item(idx int) *entity.Entity {
	if itm, ok := i.items[idx]; ok {
		return itm
	}
	return nil
}

// Cleans out items of the inventory
func (i *Inventory) Clear() { i.items = make([]Item, 0) }

// Merges another inventory into this one.
func (i *Inventory) Merge(other *Inventory) {
	// Avoid merging with oneself.
	if i == other {
		return
	}
	for _, itm := range other.Items() {
		i.Add(itm.Item, itm.Quanty)
	}
}
