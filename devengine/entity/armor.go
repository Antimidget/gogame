package entity

import (
	"encoding/json"
	"os"
)

type Armor struct {
	id          string `json:'id'`
	name        string `json:'name'`
	description string `json:'description'`
	Defense     uint   `json:'defense'`
}

func NewArmor(id, name, description string, defense uint) *Armor {
	return &Armor{id: id,
		name:        name,
		description: description,
		Defense:     defense}
}

func (a *armor) LoadJson(file string) error {
	raw, err := os.Open(file)
	if err != nil {
		return err
	}
	json.NewDecoder(raw).Decode(a)
	return nil
}

func (a *armor) Id() string          { return a.id }
func (a *armor) Name() string        { return a.name }
func (a *armor) Description() string { return a.description }

func (a *armor) String() string { return a.Id }
func (a *armor) Destroy()       {}
