package entity

import (
	"encoding/json"
	"os"
)

type Entity interface {
	Id() string
	Name() string
	Description() string
	LoadJson(file string) error
	String() string
	Destroy()
}
