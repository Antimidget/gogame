package entity

import (
	"encoding/json"
	"os"
)

type Manager struct {
	entities map[string]Entity
}

func NewManager() *Manager { return &Manager{entities: make(map[string]Entity, 0)} }

// Load a list of entities from a JSON file.
func (m *Manager) LoadJson(file string) error {
	raw, err := os.Open(file)
	if err != nil {
		return err
	}
	json.NewDecoder(raw).Decode(&m.entities)
	return nil
}

func (m *Manager) Entity(id string) Entity {
	if e, ok := m.entities[id]; ok {
		return e
	}
	return nil
}

func (m *Manager) Destroy() {}
