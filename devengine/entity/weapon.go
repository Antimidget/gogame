package entity

import (
	"encoding/json"
	"os"
)

type Weapon struct {
	id          string `json:'id'`
	name        string `json:'name'`
	description string `json:'description'`
	Damage      uint   `json:'damage'`
}

func NewWeapon(id, name, description string, damage uint) *Weapon {
	return &Weapon{id: id,
		name:        name,
		description: description,
		Damage:      damage}
}

func (w *Weapon) LoadJson(file string) error {
	raw, err := os.Open(file)
	if err != nil {
		return err
	}
	json.NewDecoder(raw).Decode(w)
	return nil
}

func (w *Weapon) Id() string          { return w.id }
func (w *Weapon) Name() string        { return w.name }
func (w *Weapon) Description() string { return w.description }

func (w *Weapon) String() string { return w.id }
func (w *Weapon) Destroy()       {}
