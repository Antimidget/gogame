package entity

import (
	"encoding/json"
	"os"
)

type Item struct {
	id          string `json:'id'`
	name        string `json:'name'`
	description string `json:'description'`
}

func NewItem(id, name, description string) *Item {
	return &Item{id: id,
		name:        name,
		description: description}
}

func (i *Item) LoadJson(file string) error {
	raw, err := os.Open(file)
	if err != nil {
		return err
	}
	json.NewDecoder(raw).Decode(i)
	return nil
}

func (i *Item) Id() string          { return i.id }
func (i *Item) Name() string        { return i.name }
func (i *Item) Description() string { return i.description }

func (i *Item) String() string { return i.Id }
func (i *Item) Destroy()       {}
