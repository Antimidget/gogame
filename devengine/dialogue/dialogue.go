package dialogue

import ()

type Dialogue struct {
	// Text thats displayed
	description string
	choices     []string
}

func New(desc string, choices ...string) *Dialogue { return nil }

func (d *Dialogue) AddChoice(c string) { d.choices = append(d.choices, c) }

// Returns the number of avaliable choices
func (d *Dialogue) Size() int { return len(d.choices) }

// Display the dialogue to the user
func (d *Dialogue) Activate() {}
